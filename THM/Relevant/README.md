# Penetration Test Report - [Granny]

## Table of Contents
- [1. Scanning](#2-scanning)
- [2. Exploitation](#3-exploitation)
- [3. Post-Exploitation](#4-post-exploitation)
- [4. Final Considerations](#5-final-considerations)

![Granny Banner](screenshots/grannybanner.png)

## 1. Scanning
To start we begin with a port scan, to check wich service are running on the target machine:

> nmap -A relevant.thm

here is the nmap output:

Starting Nmap 7.60 ( https://nmap.org ) at 2024-02-06 20:26 GMT
Nmap scan report for ip-10-10-212-223.eu-west-1.compute.internal (10.10.212.223)
Host is up (0.00047s latency).
Not shown: 995 filtered ports
PORT     STATE SERVICE       VERSION
80/tcp   open  http          Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: IIS Windows Server
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds  Windows Server 2016 Standard Evaluation 14393 microsoft-ds
3389/tcp open  ms-wbt-server Microsoft Terminal Services
| ssl-cert: Subject: commonName=Relevant
| Not valid before: 2024-02-05T19:55:08
|_Not valid after:  2024-08-06T19:55:08
|_ssl-date: 2024-02-06T20:26:55+00:00; 0s from scanner time.
MAC Address: 02:BE:E4:C4:3C:BB (Unknown)
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: general purpose
Running (JUST GUESSING): Microsoft Windows 10 (85%)
OS CPE: cpe:/o:microsoft:windows_10
Aggressive OS guesses: Microsoft Windows 10 build 14393 (85%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 1 hop
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

> nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse 10.10.143.67 > smb-enum.log

Starting Nmap 7.60 ( https://nmap.org ) at 2024-02-08 19:40 GMT
Nmap scan report for ip-10-10-143-67.eu-west-1.compute.internal (10.10.143.67)
Host is up (0.00013s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds
MAC Address: 02:02:38:4F:AA:7F (Unknown)

Host script results:
| smb-enum-shares: 
|   account_used: guest
|   \\10.10.143.67\ADMIN$: 
|     Type: STYPE_DISKTREE_HIDDEN
|     Comment: Remote Admin
|     Anonymous access: <none>
|     Current user access: <none>
|   \\10.10.143.67\C$: 
|     Type: STYPE_DISKTREE_HIDDEN
|     Comment: Default share
|     Anonymous access: <none>
|     Current user access: <none>
|   \\10.10.143.67\IPC$: 
|     Type: STYPE_IPC_HIDDEN
|     Comment: Remote IPC
|     Anonymous access: <none>
|     Current user access: READ/WRITE
|   \\10.10.143.67\nt4wrksv: 
|     Type: STYPE_DISKTREE
|     Comment: 
|     Anonymous access: <none>
|_    Current user access: READ/WRITE

Nmap done: 1 IP address (1 host up) scanned in 1.10 seconds

![Smb Share](screenshots/smbsharefound.png)

Passwords base64 encoded

[User Passwords - Encoded]
Qm9iIC0gIVBAJCRXMHJEITEyMw==
QmlsbCAtIEp1dzRubmFNNG40MjA2OTY5NjkhJCQk


Bob - !P@$$W0rD!123
Bill - Juw4nnaM4n420696969!$$$

> nmap -oA nmap-vuln -Pn -script vuln -p 445 10.10.133.136 
Starting Nmap 7.60 ( https://nmap.org ) at 2024-02-09 19:25 GMT
Nmap scan report for ip-10-10-133-136.eu-west-1.compute.internal (10.10.133.136)
Host is up (0.0043s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds
MAC Address: 02:2E:E3:BB:06:0D (Unknown)

Host script results:
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: ERROR: Script execution failed (use -d to debug)
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|_      https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|_smb-vuln-regsvc-dos: ERROR: Script execution failed (use -d to debug)

Nmap done: 1 IP address (1 host up) scanned in 16.87 seconds



## 2. Exploitation
Doing some research we tried to exploit with some described vulns about this webdav, but nothing works. The problem was: when we tried to upload some file with any extension
the application rejects our request, it only allows us to upload .txt files, because of it we tried other means, but target was not vulnerable. After a while, we realize that,
the webdav application allow us to upload the .txt but allow us to chnage it extension remotly using the cmd "MOVE".

![Webdav Move cmd](screenshots/webdavmovecmd.png)

and we got it, when requested the webshell raised on the browser.

![Webshell](screenshots/webshell.png)

Now, we want to upload our reverse shell crafted by msfvenom with .aspx extension, after it we have to upload our file and set the meterpreter to bind our reverse shell.

![Meterpreter Setup](screenshots/settingmeterpreter.png)

After craft the reverse shell as aspx extension and use curl tool to upload and "MOVE" it, then we requested it by browser and we're in.

![Inside Windows](screenshots/userowned.png)


## 3. Post-Exploitation
Now we are in, first things first, we collected the systeminfo and the OS version really was a Windows 2003 version with some hotfix implemented. You can check the sysinfo
in this [File](sys-info.txt). Ok, normal procedure, we used the Windows-Exploit-Suggester in the log of system info, and the exploits listed there, wich escal privileges tags,
for some reason didn't work. Msfconsole has these exploits stored in its database:

![ms14-009](screenshots/ms14-009wrong.png)
![ms14-058](screenshots/ms14-058options.png)

Well, lets try another way so, now we used the msfconsole exploit suggester module itself and it worked.

![Msf-exploit-suggester](screenshots/msf-exploit-suggester.png)
![ms14-070](screenshots/privexploitms14-070works.png)

As you can see, we are the NT AUTHORITY\SYSTEM. =)

## 4. Final Conclusions

**1. Scanning:**
   - The initial scanning revealed a Microsoft IIS web server running on port 80.
   - The web server was identified as having a WebDAV service with the PUT method enabled, posing a security risk.
   - Web fuzzing with BurpSuite uncovered potential vulnerabilities.

**2. Exploitation:**
   - Attempts to exploit known vulnerabilities in the WebDAV service were unsuccessful.
   - Discovered that the WebDAV service allowed file uploads only with a .txt extension, but could later change the file extension using the "MOVE" command.
   - Successfully exploited the WebDAV service to upload a web shell with a .aspx extension.
   - Crafted a reverse shell using msfvenom, uploaded it, and gained access to the system.

**3. Post-Exploitation:**
   - Conducted post-exploitation activities after gaining access to the Windows 2003 system.
   - Collected system information, confirming the OS version as Windows 2003.
   - Attempted to use Windows-Exploit-Suggester based on system information but encountered issues.
   - Successfully used the msfconsole exploit suggester module to identify and exploit the "ms14-070" vulnerability, escalating privileges to NT AUTHORITY\SYSTEM.

### Overall Assessment:
   - The initial WebDAV exploitation, though challenging, led to successful system compromise.
   - Effective use of tools like BurpSuite, msfvenom, and curl facilitated the exploitation process.
   - Troubleshooting was required during post-exploitation attempts, demonstrating adaptability.
   - The ultimate achievement of NT AUTHORITY\SYSTEM privileges indicates a thorough compromise of the target system.
