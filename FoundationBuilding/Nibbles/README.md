# Penetration Test Report - [Nibbles]

## Table of Contents
- [1. Reconnaissance](#1-reconnaissance)
- [2. Scanning](#2-scanning)
- [3. Exploitation](#3-exploitation)
- [4. Post-Exploitation](#4-post-exploitation)
- [5. Final Considerations](#5-final-considerations)

![Nibbles Banner](screenshots/nibblesbanner.png)
## 1. Reconnaissance
In CTF challenges on platforms like Hack The Box, we are provided with prior knowledge of the target machine, its IP address, and sometimes even its name and intended vulnerabilities. This level of information significantly reduces the time of the reconnaissance phase

## 2. Scanning
For active scanning we ran a port scan using nmap.

![Port Scanning](screenshots/nmapnibbles.png)

We found some interesting stuff. First, we saw that the ssh version running at 22 port was vulnerable to user enumeration. So we tried to brute-force some basic users using nse scripts.

![Vulnerable SSH Version](screenshots/userenumssh.png)
![Nmap NSE scripts](screenshots/sshbrutense.png)

After this, we got nothing. So we went to the next service, the http port. There, we found an apache service running at 80. We used the burp suite to try some content discovery and got a dir named **nibbleblog** . So, doing some research about it we got some juicy stuff.

![Burp Discovery](screenshots/burpdiscovery.png)
![Nibble Exploit Search](screenshots/nibbleblogexploitsearch.png)

## 3. Exploiting
Then, at rapid7 reference was mapped the module name to use to exploit this with metasploit.

![Metasploit instructions](screenshots/metasploitmodule.png)

While setting up the exploit settings, we realized that some credentials were needed and we didn't have it yet so, to get them we came back to the application and after a while, we did some fuzzing looking for some hidden content to get these credentials and... Bingo!

![ffufing nibbleblog](screenshots/contentdiscovery.png)

In the **install.php** module was listed a path dir which was indexed of in the apache web server and there was listed some interesting file for example a file containing the **admin** user name. After a fill default tries and doing some researches, we found that one of
the default credentials had **nibbles** as password and worked! 

![Index of](screenshots/indexofnibbles.png)
![Admin User](screenshots/usernibble.png)


and we got the user flag using msfconsole.

![User flag](screenshots/userflagnibble.png)

## 4. Post-Exploitation
The priv escal part was easier than we imagine. Right at the beginning, coincidentally just checking our privileges as nibbler user, we can see things that interest us.

![Priv Scaling](screenshots/privescal.png)

There exists a module named **monitor.sh** that is in the root's execution path with no-password needs.

![Personal Dir](screenshots/personaldirectory.png)

Checking out the content of the nibbler's homedir there was the **personal** directory. What i've done was just upload our own version of **monitor.sh** using the meterpreter, to the target machine.  The content of it was just:

>> sudo su

![Upload Monitor.sh](screenshots/privescalingbash.png)

Then, we got the root.

![Root User](screenshots/rootuserowned.png)



# 5. Final Conclusions 

In the course of our penetration testing on the 'Nibbles' machine hosted on Hack The Box, we conducted a comprehensive analysis of the target system and successfully achieved significant milestones. The key findings and conclusions are as follows:

## Initial Reconnaissance
We began by gathering essential information about the 'Nibbles' machine, which included its IP address, port scan results, and the identification of running services. This preliminary information guided our subsequent actions.

## Port Scanning and Service Enumeration
Active scanning using Nmap revealed the presence of various services, including SSH and HTTP. We identified a vulnerability in the SSH service, which allowed for user enumeration, and we attempted to brute-force basic usernames to gain access.

## HTTP Service Exploration
We shifted our attention to the HTTP service running on port 80. Using the Burp Suite, we conducted content discovery and found a directory named 'nibbleblog.' Further investigation led to the identification of potential exploits for the 'Nibbleblog' content management system.

## Exploiting and Gaining User Access
Through research and the Metasploit framework, we discovered an appropriate module to exploit the 'Nibbleblog' software. However, we needed valid credentials for successful exploitation. A fortunate discovery of an 'install.php' file revealed default credentials ('nibbles') that granted us access to the 'Nibbleblog' application.

## User Privilege Escalation
As 'nibbler' user, we identified a script named 'monitor.sh' in the root's execution path without requiring a password for execution. This script allowed us to execute privileged commands, and by uploading a modified 'monitor.sh' script using Meterpreter, we escalated our privileges to root.

## Root Access
With root privileges, we achieved complete control over the target system, and this marked the successful conclusion of our penetration test.

---
