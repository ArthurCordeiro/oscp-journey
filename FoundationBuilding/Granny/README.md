# Penetration Test Report - [Granny]

## Table of Contents
- [1. Scanning](#2-scanning)
- [2. Exploitation](#3-exploitation)
- [3. Post-Exploitation](#4-post-exploitation)
- [4. Final Considerations](#5-final-considerations)

![Granny Banner](screenshots/grannybanner.png)

## 1. Scanning
First of all we need to map the services running on the target machine. To check this we used the nmap tool:

![Port Scanning](screenshots/nmapgranny.png)

The only service running in the first 1000 ports is a web Microsoft IIS web server, running at the port 80. So lets check it out.

![HTTP service](screenshots/grannywebservice.png)

Checking it, is a webdav which is an application which allows to do some changes in the hosted site's configuration using the http protocol so, it uses more methods than the
http protocol itself with more elaborate methods. In this version running in a Windows 2003 with default configuration it allows the PUT method, and it is dangerous because it
allows a remote machine to upload any file to the root of the webserver. We get this with BurpSuite active scanning, while we were scanning with burpsuite, we used some web fuzzing
to try to discover some interesting things.

![WebFuzzing](screenshots/webfuzzing.png)
![BurpSuite Put method](screenshots/webputmethod.png)

## 2. Exploitation
Doing some research we tried to exploit with some described vulns about this webdav, but nothing works. The problem was: when we tried to upload some file with any extension
the application rejects our request, it only allows us to upload .txt files, because of it we tried other means, but target was not vulnerable. After a while, we realize that,
the webdav application allow us to upload the .txt but allow us to chnage it extension remotly using the cmd "MOVE".

![Webdav Move cmd](screenshots/webdavmovecmd.png)

and we got it, when requested the webshell raised on the browser.

![Webshell](screenshots/webshell.png)

Now, we want to upload our reverse shell crafted by msfvenom with .aspx extension, after it we have to upload our file and set the meterpreter to bind our reverse shell.

![Meterpreter Setup](screenshots/settingmeterpreter.png)

After craft the reverse shell as aspx extension and use curl tool to upload and "MOVE" it, then we requested it by browser and we're in.

![Inside Windows](screenshots/userowned.png)


## 3. Post-Exploitation
Now we are in, first things first, we collected the systeminfo and the OS version really was a Windows 2003 version with some hotfix implemented. You can check the sysinfo
in this [File](sys-info.txt). Ok, normal procedure, we used the Windows-Exploit-Suggester in the log of system info, and the exploits listed there, wich escal privileges tags,
for some reason didn't work. Msfconsole has these exploits stored in its database:

![ms14-009](screenshots/ms14-009wrong.png)
![ms14-058](screenshots/ms14-058options.png)

Well, lets try another way so, now we used the msfconsole exploit suggester module itself and it worked.

![Msf-exploit-suggester](screenshots/msf-exploit-suggester.png)
![ms14-070](screenshots/privexploitms14-070works.png)

As you can see, we are the NT AUTHORITY\SYSTEM. =)

## 4. Final Conclusions

**1. Scanning:**
   - The initial scanning revealed a Microsoft IIS web server running on port 80.
   - The web server was identified as having a WebDAV service with the PUT method enabled, posing a security risk.
   - Web fuzzing with BurpSuite uncovered potential vulnerabilities.

**2. Exploitation:**
   - Attempts to exploit known vulnerabilities in the WebDAV service were unsuccessful.
   - Discovered that the WebDAV service allowed file uploads only with a .txt extension, but could later change the file extension using the "MOVE" command.
   - Successfully exploited the WebDAV service to upload a web shell with a .aspx extension.
   - Crafted a reverse shell using msfvenom, uploaded it, and gained access to the system.

**3. Post-Exploitation:**
   - Conducted post-exploitation activities after gaining access to the Windows 2003 system.
   - Collected system information, confirming the OS version as Windows 2003.
   - Attempted to use Windows-Exploit-Suggester based on system information but encountered issues.
   - Successfully used the msfconsole exploit suggester module to identify and exploit the "ms14-070" vulnerability, escalating privileges to NT AUTHORITY\SYSTEM.

### Overall Assessment:
   - The initial WebDAV exploitation, though challenging, led to successful system compromise.
   - Effective use of tools like BurpSuite, msfvenom, and curl facilitated the exploitation process.
   - Troubleshooting was required during post-exploitation attempts, demonstrating adaptability.
   - The ultimate achievement of NT AUTHORITY\SYSTEM privileges indicates a thorough compromise of the target system.
