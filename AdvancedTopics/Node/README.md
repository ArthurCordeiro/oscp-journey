# Penetration Test Report - [Node]

## Table of Contents
- [1. Scanning](#2-scanning)
- [2. Exploitation](#3-exploitation)
- [3. Post-Exploitation](#4-post-exploitation)
- [4. Final Considerations](#5-final-considerations)

![Node Banner](screenshots/nodebanner.png)

## 1. Scanning

>> nmap -sS -Pn -sV -p 1-1000 -vv node 

>> nmap -A -O -p- -vv node 

![Port Scan Node](screenshots/nmapnode.png)

## 2. Exploitation ## 3. Post-Exploitation

## 4. Final Conclusions

**1. Scanning:**
   - The initial scanning revealed a Microsoft IIS web server running on port 80.
   - The web server was identified as having a WebDAV service with the PUT method enabled, posing a security risk.
   - Web fuzzing with BurpSuite uncovered potential vulnerabilities.

**2. Exploitation:**
   - Attempts to exploit known vulnerabilities in the WebDAV service were unsuccessful.
   - Discovered that the WebDAV service allowed file uploads only with a .txt extension, but could later change the file extension using the "MOVE" command.
   - Successfully exploited the WebDAV service to upload a web shell with a .aspx extension.
   - Crafted a reverse shell using msfvenom, uploaded it, and gained access to the system.

**3. Post-Exploitation:**
   - Conducted post-exploitation activities after gaining access to the Windows 2003 system.
   - Collected system information, confirming the OS version as Windows 2003.
   - Attempted to use Windows-Exploit-Suggester based on system information but encountered issues.
   - Successfully used the msfconsole exploit suggester module to identify and exploit the "ms14-070" vulnerability, escalating privileges to NT AUTHORITY\SYSTEM.

### Overall Assessment:
   - The initial WebDAV exploitation, though challenging, led to successful system compromise.
   - Effective use of tools like BurpSuite, msfvenom, and curl facilitated the exploitation process.
   - Troubleshooting was required during post-exploitation attempts, demonstrating adaptability.
   - The ultimate achievement of NT AUTHORITY\SYSTEM privileges indicates a thorough compromise of the target system.
