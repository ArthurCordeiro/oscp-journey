# Penetration Test Report - [Cronos]

## Table of Contents
- [1. Scanning](#2-scanning)
- [2. Final Considerations](#5-final-considerations)

![Cronos Banner](screenshots/cronosbanner.png)

## 1. Scanning
This machine explore a bunch of concepts that turns it in a very interesting target. First of all, if we try to enumarete the services running on it
some software named cadlock shows up and the return of an ordinary nmap scan results in nothing. 

![Normal Scan](screenshots/cronosnmap.png)

So we used the SYN scan and at the same time blocked the host discovery.

![SYN SCAN](screenshots/synscancronos.png)

Voila! Got some stuff to search in. At first sight, the dns service give us some clues about the target but, for a while we got stucked in the apache
web server running at 80, trying some way to exploit it. After a few searches, we found that the apache version on the target machine was vulnerable
to shellshock vuln. So, for a while we were trying to find some hidden dir or file in cgi-bin directory, but obviously without success. 

![Apache fuff](screenshots/apachefuff.png)

Then, we tried a subdomain bruteforce enum due to the DNS service, but i knew that service wasn't there for free so, after spent a good time searching 
for something, we got something.

![DNS enum](screenshots/localdnsenum.png)

Yeah, locally enumerating the dns service, we found the cronos.htb subdomain. Which gave me an insight to check the zone transfer, and again bingo! 
Finally we are in the right direction!

![Local Zone Transfer](localzonetransfer.png)

After this, a the admin.cronos.htb was discovered we need to check it.

![Cronos Admin Subdomain](screenshots/cronos-adminsubdomain.png)

Alright, after a while we tried some things. First of all i ran a nuclei into it, because nikto and arjun didn't work fine. So, with nuclei we got the
confirmation that the application works with some php version running on it, then we tried some parameter enum with arjun, as we already said and
nothing happened. 

![Parameter ENUM](screenshots/parameterenum.png)
![Nuclei Scan](screenshots/nuclei-waf.png)

Then, with burp, using the intruder module, we shoot in the SQLi fuzzing and something interesting points to the application beeing vulnerable
to blind SQLi. The nuclei got the apache-waf running on the remote host, so we got to check the time response because it was a blind attempt and for
some payloads we got no response which double check our suspects.

![Timeout Payloads](screenshots/timeoutpayloads.png)

>> Wordlists for fuzzing Seclists/Fuzzing/SQLi 

But something was strange, the application didn't respond anyway, even if we were using the mitm from burp, we had to receive at least a few answer
but the connections closes due the time limit from the TCP protocol. As you can see, all the payloads which causes the timeout response, were related
to login bypass so, we tried it manually and nothing new, the application keeps not respondig.

After checking some writeups, we were right, the application really was vulnerable to blind SQLi but it has to respond and for us it wasn't and we 
finish here. =(

